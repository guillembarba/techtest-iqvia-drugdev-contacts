# DrugDev Technical Test

The goal of this exercise is to build a fully functional API application using best
practices and reusable code.


## Rules

Your application must use the **Flask** web framework and the **SQLAlchemy**
library for interacting with the database.

You may use the built-in development server and SQLite as the database
engine.

You are free to use any other libraries you like. All dependencies should be
installable via pip and listed in a requirements.txt file.

You should **include unit tests**.


## Task

**Step 1**: Create a small contact management API with the following
functionality:

- List all contacts
- Find a contact by username
- Create a new contact
- Update a contact
- Delete a contact

At a minimum, the Contact model consists of a **username**, **first name** and
**last name**. You may add any other columns necessary to implement the
required functionality.

**Step 2**: As a second step, extend your application to support email
addresses.

Introduce a new Email model and allow a Contact to have multiple email
addresses. Adjust the endpoints above to return emails in their output and
accept emails as input as you see fit.

**Step 3**: Use Celery to add asynchronous functionality to your application.

- Implement a task that creates a Contact with random data every 15
seconds.
- Implement a task that removes Contacts older than 1 minute.

For convenience, use Redis as your Celery broker and assume it will be
installed and running on the test machine.
