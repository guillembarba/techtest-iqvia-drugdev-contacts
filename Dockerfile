FROM python:3.7-slim


## Install requirements

RUN mkdir /app-requirements
# TODO: use multistage builds to don't install development requirements in the production image
# COPY requirements.txt /app-requirements/
COPY requirements-dev.txt /app-requirements/requirements.txt

WORKDIR /app-requirements
RUN pip install --no-cache-dir --upgrade pip && \
  pip --no-cache-dir install -r /app-requirements/requirements.txt


## Preparing the app code

RUN mkdir /app
WORKDIR /app

COPY drugdev_contacts /app/drugdev_contacts
# TODO: only copy in the non-production image when implement multi-stage
COPY tests /app/tests

ENV FLASK_APP=drugdev_contacts.app

EXPOSE 5000
CMD ["flask", "run", "-h", "0.0.0.0"]
