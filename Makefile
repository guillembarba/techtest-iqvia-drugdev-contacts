.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

help:  ## print this help message
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

bserver: build server  ## build the image and run the development server

server:  ## run the development server
	docker-compose up server

cworker:  ## run the celery worker
	docker-compose up celery-worker

cbeat:  ## run the celery beat
	docker-compose up celery-beat

qa: lint test  ## run the linter and tests

lint: ## check style with flake8
	docker-compose run --rm server sh -c "flake8 drugdev_contacts tests && echo \"Linter: OK\""

test:  ## test with pytest and show coverage
	docker-compose run --rm server sh -c "pytest --no-print-logs --cov=drugdev_contacts tests && coverage report --show-missing"

test-dev:  ## run pytest in verbose mode failing in the first failure
	docker-compose run --rm server sh -c "pytest -vvv -x tests"

create-db:  ## create a new DB deleting the existing one
	docker-compose run --rm server python manage.py create-db

requirements-compile: ## compile requirements .txt files from the .in
	docker-compose run --rm --no-deps server sh -c "pip-compile --upgrade requirements.in && pip-compile --upgrade requirements-dev.in"
	sudo chown $$USER. requirements*.txt

fshell:  ## run the flask shell
	docker-compose run --rm server python manage.py shell

shell:  ## run a shell inside the server's container where, for example, you can play with all commands available in "python manage.py"
	docker-compose run --rm server sh

build: clean  ## rebuild the image what implies install the requirements
	docker-compose build

clean:  ## remove files
	sudo rm -r **/__pycache__
