from datetime import datetime

import pytest
from flask_sqlalchemy import event
from freezegun import freeze_time

from drugdev_contacts.models import Contact, Email, db

FREEZED_ISOFORMAT_TIME = '2019-09-20T13:53:17'


@pytest.fixture
def mock_db_current_timestamp(mocker):

    with freeze_time(FREEZED_ISOFORMAT_TIME, tick=False) as frozen_time:
        def set_timestamp(mapper, connection, target):
            now = datetime.now()
            if hasattr(target, 'created'):
                target.created = now
            if hasattr(target, 'updated'):
                target.updated = now
        event.listen(Contact, 'before_insert', set_timestamp, propagate=True)
        yield frozen_time
        event.remove(Contact, 'before_insert', set_timestamp)


@pytest.fixture
def no_contacts():
    return []


@pytest.fixture
def one_contact(app, mock_db_current_timestamp):
    contact = Contact(
        username="existing",
        first_name='I',
        last_name='Exist',
    )
    contact.emails = [
        Email(
            email_address='existing@email.com',
            description='main',
        ),
        Email(
            email_address='existing@work.com',
            description='work',
        ),
    ]

    db.session.add(contact)
    db.session.commit()

    return [contact]


@pytest.fixture
def multiple_contacts(one_contact, app, mock_db_current_timestamp):
    contacts = []

    contact2 = Contact(
        username="existing2",
        first_name='Pepe',
        last_name='Grillo',
    )
    contacts.append(contact2)

    contact3 = Contact(
        username="existing3",
        first_name='John',
        last_name='Doe',
    )
    contact3.emails = [
        Email(
            email_address='existing3.1@email.com',
            description='main',
        ),
    ]
    contacts.append(contact3)

    contact4 = Contact(
        username="existing4",
        first_name='Lion',
        last_name='King',
    )
    contacts.append(contact4)

    db.session.add_all(contacts)
    db.session.commit()

    return one_contact + contacts


@pytest.fixture
def valid_values():
    return {
        'username': 'myusername',
        'first_name': 'Guillem',
        'last_name': 'Barba',
    }


def test_list_contacts_response(one_contact, client):
    # given ... one contact in the system
    # when
    response = client.get('/contacts/')
    # then
    assert response.status_code == 200, f'{response}, {response.data}'
    assert response.get_json() == [{
        'username': 'existing',
        'first_name': 'I',
        'last_name': 'Exist',
        'created': FREEZED_ISOFORMAT_TIME,
        'emails': [
            {
                'email_address': 'existing@email.com',
                'description': 'main',
            },
            {
                'email_address': 'existing@work.com',
                'description': 'work',
            },
        ]
    }]


@pytest.mark.parametrize('existing_contacts_fixname, expected_n_contacts', (
    ('no_contacts', 0),
    ('one_contact', 1),
    ('multiple_contacts', 4),
))
def test_list_all_contacts(existing_contacts_fixname, expected_n_contacts, client, request):
    """Test it returns all the contacts in the system."""
    # given ... some contacts in the system
    existing_contacts = request.getfixturevalue(existing_contacts_fixname)
    # when
    response = client.get('/contacts/')

    # then
    assert response.status_code == 200, f'{response}, {response.data}'

    returned_contacts = response.get_json()
    assert len(returned_contacts) == expected_n_contacts

    expected_usernames = set(c.username for c in existing_contacts)
    assert set(x['username'] for x in returned_contacts) == expected_usernames


def test_find_existing_contact(multiple_contacts, client):
    # given ... several contacts in the system
    # when
    searched_username = 'existing'
    response = client.get(f'/contacts/?username={searched_username}')

    # then
    assert response.status_code == 200, f'{response}, {response.data}'
    returned_contacts = response.get_json()
    assert len(returned_contacts) == 1
    assert returned_contacts[0]['username'] == searched_username


def test_find_nonexisting_contact(multiple_contacts, client):
    # given ... several contacts in the system
    # when
    searched_username = 'nonexisting'
    response = client.get(f'/contacts/?username={searched_username}')

    # then
    assert response.status_code == 200, f'{response}, {response.data}'
    returned_contacts = response.get_json()
    assert len(returned_contacts) == 0


def test_create_valid_contact(valid_values, client, mock_db_current_timestamp):
    # given ... valid contact values
    # when
    response = client.post('/contacts/', json=valid_values)

    # then
    # ... the request is success
    assert response.status_code == 201, f'{response}, {response.data}'
    # ... it returns the created contact details with and empty list in emails
    new_contact_values = response.get_json()
    valid_values['emails'] = []
    valid_values['created'] = FREEZED_ISOFORMAT_TIME
    assert new_contact_values == valid_values
    # ... the new contact is in the DB
    db_contact = Contact.query.filter_by(username=valid_values['username']).first()
    assert db_contact
    assert db_contact.first_name == valid_values['first_name']
    assert db_contact.last_name == valid_values['last_name']


def test_create_contact_existing_username(multiple_contacts, valid_values, client):
    # given
    # ... some contacts in the system
    # ... valid contact values but with already existing username
    valid_values['username'] = 'existing'
    # when
    response = client.post('/contacts/', json=valid_values)
    # then
    assert response.status_code == 400, f'{response}, {response.data}'
    assert response.get_json() == {
        'username': [
            'This username already exist.',
        ]
    }


@pytest.mark.parametrize('missing_fields', (
    ('username',),
    ('first_name',),
    ('last_name',),
    ('username', 'first_name'),
    ('username', 'first_name', 'last_name'),
))
def test_create_contact_missing_fields(multiple_contacts, valid_values, missing_fields, client):
    # given
    # ... some contacts in the system
    # ... contact values with some missing fields
    expected_response = {}
    for fname in missing_fields:
        del valid_values[fname]
        expected_response[fname] = [
            'Missing data for required field.',
        ]

    # when
    response = client.post('/contacts/', json=valid_values)
    # then
    assert response.status_code == 400, f'{response}, {response.data}'
    assert response.get_json() == expected_response


@pytest.mark.parametrize('values_to_update', (
    {},
    {  # same existing values
        'username': 'existing',
        'first_name': 'I',
        'last_name': 'Exist',
    },
    {
        'username': 'anotherusername',
    },
    {
        'first_name': 'William',
    },
    {
        'last_name': 'Barba Domingo',
    },
    {
        'first_name': 'William2',
        'last_name': 'Barba2',
    },
    {
        'username': 'anotherusername3',
        'first_name': 'William3',
        'last_name': 'Barba3',
    },
))
def test_update_existing_username_valid_data(multiple_contacts, values_to_update, client):
    # given
    # ... some contacts in the system
    # ... the current values for an existing contact
    username_to_update = 'existing'
    current_values = {
        'username': username_to_update,
        'first_name': 'I',
        'last_name': 'Exist',
    }
    # when
    response = client.patch(f'/contacts/{username_to_update}/', json=values_to_update)

    # then
    assert response.status_code == 200, f'{response}, {response.data}'
    # ... it returns the full updated contact details
    current_values.update(values_to_update)
    current_values['created'] = FREEZED_ISOFORMAT_TIME
    returned_values = response.get_json()
    # ... the contact's emails are returned
    assert 'emails' in returned_values
    del returned_values['emails']
    assert returned_values == current_values
    # ... the values in the DB has been updated
    db_contact = Contact.query.filter_by(username=current_values['username']).first()
    assert db_contact.first_name == current_values['first_name']
    assert db_contact.last_name == current_values['last_name']


def test_update_contact_username_to_existing_username(multiple_contacts, client):
    # given ... some contacts in the system
    # when
    username_to_update = 'existing2'
    response = client.patch(f'/contacts/{username_to_update}/', json={
        'username': 'existing',
    })
    # then
    assert response.status_code == 400, f'{response}, {response.data}'
    assert response.get_json() == {
        'username': [
            f'Another contact with this username already exist.',
        ],
    }


def test_update_nonexisting_contact(multiple_contacts, client):
    # given ... some contacts in the system
    # when
    username_to_update = 'nonexisting'
    response = client.patch(f'/contacts/{username_to_update}/', json={
        'first_name': 'William',
    })
    # then
    assert response.status_code == 404, f'{response}, {response.data}'
    assert response.get_json() == {
        'message': f'The contact with username "{username_to_update}" doesn\'t exist.',
    }


def test_delete_existing_contact(multiple_contacts, client):
    # given ... some contacts in the system
    # when
    username_to_delete = 'existing'
    response = client.delete(f'/contacts/{username_to_delete}/')
    # then
    assert response.status_code == 200, f'{response}, {response.data}'
    assert not Contact.query.filter_by(username=username_to_delete).count()


def test_delete_nonexisting_contact(multiple_contacts, client):
    # given ... some contacts in the system
    # when
    username_to_delete = 'nonexisting'
    response = client.delete(f'/contacts/{username_to_delete}/')
    # then
    assert response.status_code == 404, f'{response}, {response.data}'
    assert response.get_json() == {
        'message': f'The contact with username "{username_to_delete}" doesn\'t exist.',
    }


def test_add_valid_email(multiple_contacts, client):
    # given
    # ... some contacts in the system
    # ... the current values for an existing contact
    username_to_update = 'existing'
    new_email_values = {
        'email_address': 'new@email.com',
        'description': 'Home',
    }
    # when
    response = client.post(f'/contacts/{username_to_update}/emails/', json=new_email_values)

    # then
    assert response.status_code == 201, f'{response}, {response.data}'
    # ... it returns the new e-mail values including the contact's username
    new_email_values['username'] = username_to_update
    assert response.get_json() == new_email_values
    # ... the e-mail has been added to the DB
    db_contact = Contact.query.filter_by(
        username=username_to_update,
    ).first()
    assert len(db_contact.emails) == 3
    db_email = Email.query.filter_by(
        contact_username=username_to_update,
        email_address=new_email_values['email_address'],
    ).first()
    assert db_email.description == new_email_values['description']


def test_update_existing_email(multiple_contacts, client):
    # given
    # ... some contacts in the system
    # ... the details of the e-mail to update and the new description
    username_to_update = 'existing'
    email_to_update = 'existing@email.com'
    new_values = {
        'description': 'home',
    }
    # when
    response = client.patch(
        f'/contacts/{username_to_update}/emails/{email_to_update}/',
        json=new_values,
    )

    # then
    assert response.status_code == 200, f'{response}, {response.data}'
    # ... it returns the updated e-mail values
    assert response.get_json() == {
        'username': username_to_update,
        'email_address': email_to_update,
        'description': 'home',
    }
    # ... the e-mail has been updated to the DB
    db_email = Email.query.filter_by(
        contact_username=username_to_update,
        email_address=email_to_update,
    ).first()
    assert db_email.description == 'home'


def test_delete_existing_email(multiple_contacts, client):
    # given
    # ... some contacts in the system
    # ... the details of the e-mail to delete
    username_to_update = 'existing'
    email_to_delete = 'existing@email.com'
    # when
    response = client.delete(f'/contacts/{username_to_update}/emails/{email_to_delete}/')

    # then
    # ... the request is success
    assert response.status_code == 200, f'{response}, {response.data}'
    # ... the e-mail has been deleted from the DB
    db_contact = Contact.query.filter_by(
        username=username_to_update,
    ).first()
    assert len(db_contact.emails) == 1
    assert Email.query.filter_by(
        contact_username=username_to_update,
        email_address=email_to_delete,
    ).count() == 0
