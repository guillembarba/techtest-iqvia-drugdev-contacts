import pytest

from drugdev_contacts.app import create_app
from drugdev_contacts.models import db


@pytest.fixture
def app():
    app = create_app(app_settings='drugdev_contacts.config.TestingConfig')
    with app.app_context():
        db.create_all()
        yield app
        db.session.remove()
        db.drop_all()


@pytest.fixture
def client(app):
    return app.test_client()
