# DrugDev Contact's management app

Flask + SQLAlchemy app that implements the API following the [instructions](INSTRUCTIONS.md).

Got to each *git tag* to see the result of each test.

You can use the makefile actions (just run `make` to see all available).

To run for the first time the server you will need to create the DB (`make create-db`).


## Notes about the unit tests

Given the simplicity of the application, specially in the domain (any business logic) the tests are only implemented at view level.

If it would be more business logic it should be separate unit tests for the domain (models.py) layer and view/API layer.

Also because of the simplicity and size of the application, I didn't mocked the DB layer but just used SQLite in memory (that performs really well). For bigger applications (more tests, more heavy DB layer...) or more complex stacks the DB layer and other *external* services should be mocked in most of the tests.

For time constraints I only implemented the *happy path* for the e-mail endpoints, but the failure and edge case tests should be implemented as they have been implemented for contact endpoints.

It hasn't implemented tests to check how unexpected data is dropped (ignored) when come in the payload or validation (what is done by the Marshmallow schema validation). Better if there is some tests for this.

I neither implemented tests for the *step-3* work as there isn't really nothing new to show implementing them (I adapted the existing ones what shows how to deal with datetimes in tests).
