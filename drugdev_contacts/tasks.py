import logging
from datetime import datetime, timedelta
from random import randint

from faker import Faker

from .app import create_celery
from .exceptions import ExistingContactError, ExistingEmailError
from .models import Contact, add_contact, add_email

celery = create_celery()
logger = logging.getLogger(__name__)
fake = Faker()


@celery.task()
def create_random_contact():
    logger.info('Start creating random contact...')
    try:
        contact = add_contact({
            'username': fake.profile(fields=['username'])['username'],
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
        })
    except ExistingContactError:
        logger.info('Random contact not created as generated an already existing username.')
        return

    for _ in range(randint(0, 4)):
        try:
            add_email(contact.username, {
                'email_address': fake.email(),
                'description': fake.sentence(),
            })
        except ExistingEmailError:
            logger.info('Random e-mail not created as generated already existing e-mail.')


@celery.task()
def delete_old_contacts(older_than=timedelta(seconds=60)):
    logger.info(f'Start deleting contacts older than {older_than}...')
    older_than_dt = datetime.now() - older_than
    n_deletes = Contact.query.filter(Contact.created < older_than_dt).delete()
    Contact.query.session.commit()
    logger.info(f'Deleted {n_deletes} contacts created before {older_than_dt}')
