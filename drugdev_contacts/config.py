import os
from datetime import timedelta

BASEDIR = os.path.abspath(os.path.dirname(__file__))


class BaseConfig:
    """Base configuration."""

    APP_NAME = os.getenv("APP_NAME", "DrugDev Contacts")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    CELERYBEAT_SCHEDULE = {
        'create-random-contact-every-15-minutes': {
            'task': 'drugdev_contacts.tasks.create_random_contact',
            # 'schedule': timedelta(seconds=15 * 60),
            'schedule': timedelta(seconds=35),
        },
    }


class DevelopmentConfig(BaseConfig):
    """Development configuration."""

    ENV = 'development'
    DEBUG = os.getenv('DEBUG', True)
    SQLALCHEMY_DATABASE_URI = os.getenv(
        "DATABASE_URL",
        "sqlite:///{}".format(os.path.join(BASEDIR, "dev.db")),
    )
    CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL")


class TestingConfig(BaseConfig):
    """Testing configuration."""

    ENV = 'testing'
    DEBUG = os.getenv('DEBUG', True)
    TESTING = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_TEST_URL", "sqlite:///")
    CELERY_BROKER_URL = os.getenv(
        "CELERY_TEST_BROKER_URL",
        "redis://localhost:6379",
    )
