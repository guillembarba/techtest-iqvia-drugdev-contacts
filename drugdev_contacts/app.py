import logging
import os

from celery import Celery
from flask import Flask


def create_app(app_settings=None):
    # instantiate the app
    app = Flask('drugdev-contacts')

    # set config
    if app_settings is None:
        app_settings = os.getenv("APP_SETTINGS")
        assert app_settings, 'Missing required environment variable "APP_SETTINGS"'
    app.config.from_object(app_settings)

    if app.config['DEBUG']:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    # set up extensions
    from .models import db

    db.init_app(app)

    # # register blueprints
    from drugdev_contacts.contacts.views import contacts_blueprint

    app.register_blueprint(contacts_blueprint)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {
            "app": app,
            "db": db,
        }

    return app


def create_celery(app=None):
    if app is None:
        app = create_app()

    celery = Celery(
        __name__,
        broker=app.config['CELERY_BROKER_URL'],
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery
