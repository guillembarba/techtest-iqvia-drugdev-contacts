from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import FlushError

from .exceptions import (
    ExistingContactError,
    ExistingEmailError,
    NonExistingContactError,
    NonExistingEmailError,
)

db = SQLAlchemy()


class Contact(db.Model):

    __tablename__ = "contacts"

    username = db.Column(db.String(255), primary_key=True)
    first_name = db.Column(db.String(), nullable=False)
    last_name = db.Column(db.String(), nullable=False)
    created = db.Column(db.DateTime, default=db.func.current_timestamp())
    emails = db.relationship(
        "Email",
        back_populates='contact',
        cascade="all, delete, delete-orphan",
    )

    def __repr__(self):
        return f"<Contact {self.username}>"


def add_contact(contact_data: str) -> None:
    contact = Contact(**contact_data)
    db.session.add(contact)
    try:
        db.session.commit()
    except (FlushError, IntegrityError):
        raise ExistingContactError(contact.username)
    return contact


def update_contact(username: str, new_values: dict) -> Contact:
    contact_query = Contact.query.filter_by(username=username)
    if new_values:
        try:
            n_updated = contact_query.update(new_values, synchronize_session=False)
        except IntegrityError:
            raise ExistingContactError(new_values['username'])
        if not n_updated:
            raise NonExistingContactError(username)
        Contact.query.session.commit()

    if 'username' in new_values:
        return Contact.query.filter_by(username=new_values['username']).first()
    return contact_query.first()


def delete_contact(username: str) -> None:
    n_deleted = Contact.query.filter_by(username=username).delete()
    if not n_deleted:
        raise NonExistingContactError(username)
    Contact.query.session.commit()


class Email(db.Model):

    __tablename__ = "emails"
    __table_args__ = (
        db.UniqueConstraint('contact_username', 'email_address', name='unique_contact_email'),
    )

    id = db.Column(db.Integer, primary_key=True)
    contact_username = db.Column(db.String, db.ForeignKey('contacts.username'))
    contact = db.relationship("Contact", back_populates="emails")
    email_address = db.Column(db.String, nullable=False)
    description = db.Column(db.String, nullable=False)


def add_email(username: str, email_data: dict) -> None:
    if not Contact.query.filter_by(username=username).count():
        raise NonExistingContactError(username)

    email = Email(contact_username=username, **email_data)
    db.session.add(email)
    try:
        db.session.commit()
    except (FlushError, IntegrityError):
        raise ExistingEmailError(username, email_data['email_address'])
    return email


def update_email(username: str, email_address: str, new_values: dict) -> Contact:
    if not Contact.query.filter_by(username=username).count():
        raise NonExistingContactError(username)

    email_query = Email.query.filter_by(
        contact_username=username,
        email_address=email_address,
    )
    if new_values:
        try:
            n_updated = email_query.update(new_values, synchronize_session=False)
        except IntegrityError:
            raise ExistingEmailError(username, new_values['email_address'])
        if not n_updated:
            raise NonExistingEmailError(username, email_address)
        Email.query.session.commit()

    if 'email_address' in new_values:
        return Email.query.filter_by(
            contact_username=username,
            email_address=new_values['email_address'],
        ).first()
    return email_query.first()


def delete_email(username: str, email_address: str) -> None:
    if not Contact.query.filter_by(username=username).count():
        raise NonExistingContactError(username)

    n_deleted = Email.query.filter_by(
        contact_username=username,
        email_address=email_address,
    ).delete()
    if not n_deleted:
        raise NonExistingEmailError(username)
    Email.query.session.commit()
