"""Custom exceptions."""


class ExistingContactError(Exception):
    def __init__(self, username):
        self.username = username
        super().__init__(f'Already exist contact with username="{username}".')


class NonExistingContactError(Exception):
    def __init__(self, username):
        self.username = username
        super().__init__(f'Contact with username="{username}" doesn\'t exist.')


class ExistingEmailError(Exception):
    def __init__(self, username, email_address):
        self.username = username
        super().__init__(
            f'Already exist an e-mail with address "{email_address}" in contact "{username}".',)


class NonExistingEmailError(Exception):
    def __init__(self, username, email_address):
        self.username = username
        self.email_address = email_address
        super().__init__(
            f'E-mail with address "{email_address}" in contact "{username}" doesn\'t exist.',
        )
