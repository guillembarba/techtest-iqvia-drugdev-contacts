from flask import Blueprint, jsonify, make_response, request
from marshmallow import Schema, ValidationError, fields

from drugdev_contacts.exceptions import (
    ExistingContactError,
    ExistingEmailError,
    NonExistingContactError,
    NonExistingEmailError,
)
from drugdev_contacts.models import (
    Contact,
    Email,
    add_contact,
    add_email,
    delete_contact,
    delete_email,
    update_contact,
    update_email,
)

contacts_blueprint = Blueprint("contacts", __name__)


class ContactSchema(Schema):
    username = fields.String(required=True)
    first_name = fields.String(required=True)
    last_name = fields.String(required=True)


def serialize_contact(contact: Contact) -> dict:
    return {
        'username': contact.username,
        'first_name': contact.first_name,
        'last_name': contact.last_name,
        'created': contact.created.isoformat(),
        'emails': [serialize_email(e, include_username=False) for e in contact.emails]
    }


class EmailSchema(Schema):
    email_address = fields.Email(required=True)
    description = fields.String(required=True)


def serialize_email(email: Email, include_username: bool = True) -> dict:
    res = {
        'email_address': email.email_address,
        'description': email.description,
    }
    if include_username:
        res['username'] = email.contact_username
    return res


@contacts_blueprint.route("/contacts/", methods=["GET"])
def list_contacts_view():
    if request.args.get('username'):
        contacts = Contact.query.filter_by(username=request.args['username'])
    else:
        contacts = Contact.query
    return jsonify([serialize_contact(c) for c in contacts])


@contacts_blueprint.route("/contacts/", methods=["POST"])
def create_contact_view():
    try:
        contact_data = ContactSchema().load(request.json)
    except ValidationError as err:
        return make_response(
            jsonify(err.messages),
            400,
        )

    try:
        contact = add_contact(contact_data)
    except ExistingContactError:
        return make_response(
            jsonify({
                'username': [
                    'This username already exist.',
                ]
            }),
            400,
        )

    return make_response(jsonify(serialize_contact(contact)), 201)


@contacts_blueprint.route("/contacts/<username>/", methods=["PATCH"])
def update_contact_view(username):
    try:
        contact_data = ContactSchema(partial=True).load(request.json)
    except ValidationError as err:
        return make_response(
            jsonify(err.messages),
            400,
        )

    try:
        updated_contact = update_contact(username, contact_data)
    except NonExistingContactError:
        return make_response(
            jsonify({
                'message': f'The contact with username "{username}" doesn\'t exist.',
            }),
            404,
        )
    except ExistingContactError:
        return make_response(
            jsonify({
                'username': [
                    'Another contact with this username already exist.',
                ]
            }),
            400,
        )

    return jsonify(serialize_contact(updated_contact))


@contacts_blueprint.route("/contacts/<username>/", methods=["DELETE"])
def delete_contact_view(username):
    try:
        delete_contact(username)
    except NonExistingContactError:
        return make_response(
            jsonify({
                'message': f'The contact with username "{username}" doesn\'t exist.',
            }),
            404,
        )
    return jsonify({
        'message': 'OK',
    })


@contacts_blueprint.route("/contacts/<username>/emails/", methods=["POST"])
def create_email_view(username):
    try:
        email_data = EmailSchema().load(request.json)
    except ValidationError as err:
        return make_response(
            jsonify(err.messages),
            400,
        )

    try:
        email = add_email(username, email_data)
    except NonExistingContactError:
        return make_response(
            jsonify({
                'message': f'The contact with username "{username}" doesn\'t exist.',
            }),
            404,
        )
    except ExistingEmailError:
        return make_response(
            jsonify({
                'email_address': [
                    'Another e-mail with the same address already exist for '
                    f'contact "{username}".',
                ]
            }),
            400,
        )

    return make_response(
        jsonify(serialize_email(email)),
        201,
    )


@contacts_blueprint.route("/contacts/<username>/emails/<email_address>/", methods=["PATCH"])
def update_email_view(username: str, email_address: str):
    try:
        email_data = EmailSchema(partial=True).load(request.json)
    except ValidationError as err:
        return make_response(
            jsonify(err.messages),
            400,
        )

    try:
        updated_email = update_email(username, email_address, email_data)
    except NonExistingContactError:
        return make_response(
            jsonify({
                'message': f'The contact with username "{username}" doesn\'t exist.',
            }),
            404,
        )
    except NonExistingEmailError:
        return make_response(
            jsonify({
                'message': f'The e-mail with address "{email_address}" in contact with username '
                           f'"{username}" doesn\'t exist.',
            }),
            404,
        )
    except ExistingEmailError:
        return make_response(
            jsonify({
                'username': [
                    'Another e-mail with this address already exist for this contact.',
                ]
            }),
            400,
        )

    return jsonify(serialize_email(updated_email))


@contacts_blueprint.route("/contacts/<username>/emails/<email_address>/", methods=["DELETE"])
def delete_email_view(username: str, email_address: str):
    try:
        delete_email(username, email_address)
    except NonExistingContactError:
        return make_response(
            jsonify({
                'message': f'The contact with username "{username}" doesn\'t exist.',
            }),
            404,
        )
    except NonExistingEmailError:
        return make_response(
            jsonify({
                'message': f'The e-mail with address "{email_address}" in contact '
                           f'with username "{username}" doesn\'t exist.',
            }),
            404,
        )
    return jsonify({
        'message': 'OK',
    })


@contacts_blueprint.route("/delete-old-contacts/")
def delete_old_contacts_view():
    from drugdev_contacts.tasks import delete_old_contacts

    delete_old_contacts.delay()
    return jsonify({
        'message': 'Asynchronous task launched.',
    })
