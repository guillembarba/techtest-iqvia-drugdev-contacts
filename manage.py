from flask.cli import FlaskGroup

from drugdev_contacts.app import create_app
from drugdev_contacts.models import db

# app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command()
def create_db():
    """Create a new DB deleting the existing one."""
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command()
def drop_db():
    """Drops the db tables."""
    db.drop_all()


if __name__ == "__main__":
    cli()
